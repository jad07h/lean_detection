# lean_detection
  
Thanks to:  
https://www.pyimagesearch.com/2017/04/24/eye-blink-detection-opencv-python-dlib/  
  
1) Install Python3 and pip (`See Install Python.jpg`)  
2) Install OpenCV https://sourceforge.net/projects/opencvlibrary/files/3.4.6/opencv-3.4.6-vc14_vc15.exe/download  
3) Open CMD Prompt (issue them 1 at a time incase one fails, send me output if it does)  
`pip install opencv-python;`  
`pip install cmake;`  
`pip install scipy;`  
`pip install keyboard;`  
`pip install imutils;`  
4) Install Visual Studio Build tools. Install "Linux Development with C++": https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=15#  
5) In CMD Prompt again, run: `pip install dlib` (`brew install dlib` on mac) 
  
  
# Run Instructions  
`python3 lean_detection.py --shape-predictor shape_predictor_68_face_landmarks.dat`  
OR  
`python lean_detection.py --shape-predictor shape_predictor_68_face_landmarks.dat`  
  
  
Leaning left: `[`  
Leaning right: `]`  

You'll want to verify this by using your leaning to set the key binds in PUBG or another game.
Some webcams are flipped :/  
  
# To Quit  
Click on the screen with the video stream and hit `q`  