from scipy.spatial import distance as dist
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils import face_utils
import time
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2
import keyboard


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--shape-predictor", default="shape_predictor_68_face_landmarks.dat",
  help="path to facial landmark predictor")
ap.add_argument("-v", "--video", type=str, default="",
  help="path to input video file")
args = vars(ap.parse_args())

# initialize the frame counters and the total number of blinks
COUNTER = 0
TOTAL = 0
IS_LEANING = False

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
print("[INFO] loading facial landmark predictor...")
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(args["shape_predictor"])

# grab the indexes of the facial landmarks for the left and
# right eye, respectively
(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

# start the video stream thread
print("[INFO] starting video stream thread...")
vs = VideoStream(src=0).start()

# loop over frames from the video stream
while True:
  time.sleep(0.0625)

  # grab the frame from the threaded video file stream, resize
  frame = vs.read()
  frame = imutils.resize(frame, width=300)
  gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

  # detect faces in the grayscale frame
  rects = detector(gray, 0)

  # loop over the face detections
  for rect in rects:
    # determine the facial landmarks for the face region, then
    # convert the facial landmark (x, y)-coordinates to a NumPy
    # array
    shape = predictor(gray, rect)
    shape = face_utils.shape_to_np(shape)

    # extract the left and right eye coordinates, then use the
    # coordinates to compute the eye aspect ratio for both eyes
    leftEye = shape[lStart:lEnd]
    rightEye = shape[rStart:rEnd]

    # compute difference in height between 2 eyes, treat the delta
    # like an old fashion level tool
    eye_height_delta = leftEye[0][1] - rightEye[-1][1]
    if IS_LEANING and eye_height_delta > -5 and eye_height_delta < 5:
      TOTAL = "NO LEAN"
      keyboard.release('[')
      keyboard.release(']')
      IS_LEANING = False
    elif not IS_LEANING and eye_height_delta < -5:
      # LEANING RIGHT
      TOTAL = "RIGHT"
      keyboard.press(']')
      IS_LEANING = True
    elif not IS_LEANING and leftEye[0][1] - rightEye[-1][1] > 5:
      # LEANING LEFT
      TOTAL = "LEFT"
      keyboard.press('[')
      IS_LEANING = True


  # display lean direction
  frame = cv2.flip(frame, 1)
  cv2.putText(frame, "Lean: {}".format(TOTAL), (10, 30),
    cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.4, (0, 0, 255), 1)
  # show the frame
  cv2.imshow("Frame", frame)
  key = cv2.waitKey(1) & 0xFF
 
  # if the `q` key was pressed, break from the loop
  if key == ord("q"):
    break

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()